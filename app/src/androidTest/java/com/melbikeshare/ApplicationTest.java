package com.melbikeshare;

import android.test.ActivityInstrumentationTestCase2;

import com.melbikeshare.network.NetworkCalls;
import com.robotium.solo.Solo;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ActivityInstrumentationTestCase2<MainActivity>
{
    Solo solo;

    public ApplicationTest()
    {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testValidServerResponse()
    {
        try
        {
            String result = NetworkCalls.getRequest(NetworkCalls.SODA_API_ALL_STATIONS);
            JSONArray object = new JSONArray(result);
            assertTrue(object.length() > 0);
            assertTrue(object.getJSONObject(0).has("coordinates"));
            assertTrue(object.getJSONObject(0).has("featurename"));
            assertTrue(object.getJSONObject(0).has("nbemptydoc"));
            assertTrue(object.getJSONObject(0).has("id"));
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
}