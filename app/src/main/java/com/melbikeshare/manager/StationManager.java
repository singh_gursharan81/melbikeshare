package com.melbikeshare.manager;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.melbikeshare.database.helper.DBHelperStation;
import com.melbikeshare.model.Station;
import com.melbikeshare.network.NetworkCalls;

import de.greenrobot.event.EventBus;

/**
 * Created by gursharan_singh on 8/11/2015.
 */
public final class StationManager
{
    private static final int REQUEST_TYPE_ALL_STATIONS = 0;

    private StationManager(){}

    public static void fetchAllStations()
    {
        runAsyncTask(REQUEST_TYPE_ALL_STATIONS);
    }

    private static void runAsyncTask(final int requestType)
    {
        new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground(Void... voids)
            {
                String result = null;
                switch(requestType)
                {
                    case REQUEST_TYPE_ALL_STATIONS:
                        result = NetworkCalls.getRequest(NetworkCalls.SODA_API_ALL_STATIONS);
                        Station[] stations = null;
                        try
                        {
                            if(result != null)
                            {
                                GsonBuilder builder = new GsonBuilder();
                                Gson gson = builder.create();
                                stations = gson.fromJson(result, Station[].class);
                                DBHelperStation.addStations(stations);
                            }
                            else
                            {
                                result = "An error occurred, please try again...";
                            }
                        }
                        catch(JsonSyntaxException e)
                        {
                            e.printStackTrace();
                        }

                        EventBus.getDefault().post(new EventStationsDownloaded(stations, result));
                }
                return null;
            }
        }.execute();
    }

    public static final class EventStationsDownloaded
    {
        public Station[] stations;
        public String errorMessage;
        public EventStationsDownloaded(Station[] stations, String errorMessage)
        {
            this.stations = stations;
            this.errorMessage = errorMessage;
        }
    }
}
