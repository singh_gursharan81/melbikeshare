package com.melbikeshare.database.tables;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public final class TableStation
{
    private TableStation(){}

    public static final String TABLE_NAME = "station";
    public static final String ID = "_id";
    public static final String STATION_NAME = "stationName";
    public static final String BIKE_COUNT = "bikeCount";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String[] columns =
            {
                    ID, STATION_NAME, BIKE_COUNT, LATITUDE, LONGITUDE
            };

    public static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " +
                    TABLE_NAME + " (" +
                    ID + " INTEGER PRIMARY KEY, " +
                    STATION_NAME + " VARCHAR, " +
                    BIKE_COUNT + " INTEGER, " +
                    LATITUDE + " REAL, " +
                    LONGITUDE + " REAL)";
}
