package com.melbikeshare.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.melbikeshare.MelBikeShare;
import com.melbikeshare.database.tables.TableStation;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public final class DatabaseAccess extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "MelBikeShareDB";
    private static final int DATABASE_VERSION = 1;

    private static DatabaseAccess instance;

    private DatabaseAccess(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseAccess getInstance()
    {
        if (instance == null)
            instance = new DatabaseAccess(MelBikeShare.getInstance());
        return instance;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {

    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(TableStation.CREATE_TABLE);
    }
}
