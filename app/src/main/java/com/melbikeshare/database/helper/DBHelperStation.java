package com.melbikeshare.database.helper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.google.android.gms.maps.model.LatLng;
import com.melbikeshare.database.DatabaseAccess;
import com.melbikeshare.database.tables.TableStation;
import com.melbikeshare.model.Station;
import com.melbikeshare.util.Preferences;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public final class DBHelperStation
{
    private DBHelperStation(){}

    public static void saveStation(Station station)
    {
        ContentValues values = new ContentValues();
        values.put(TableStation.BIKE_COUNT, station.getBikeCount());
        values.put(TableStation.ID, station.getId());
        values.put(TableStation.LATITUDE, station.getLocation().latitude);
        values.put(TableStation.LONGITUDE, station.getLocation().longitude);
        values.put(TableStation.STATION_NAME, station.getStationName());

        if(stationExists(station.getId()))
        {
            DatabaseAccess
                    .getInstance()
                    .getWritableDatabase()
                    .update(TableStation.TABLE_NAME, values, TableStation.ID + " = ?", new String[]{String.valueOf(station.getId())});
        }
        else
        {
            DatabaseAccess
                    .getInstance()
                    .getWritableDatabase()
                    .insert(TableStation.TABLE_NAME, null, values);
        }
    }

    public static Cursor searchWithKeywordCursor(String key)
    {
        return DatabaseAccess
                .getInstance()
                .getReadableDatabase()
                .query(TableStation.TABLE_NAME, null, TableStation.STATION_NAME + " LIKE ?",
                       new String[]{"%" + key + "%"}, null, null, null, "5");
    }

    public static List<Station> getAllStations()
    {
        Cursor cursor =
                DatabaseAccess
                .getInstance()
                .getReadableDatabase()
                .query(TableStation.TABLE_NAME, null, null, null, null, null, null);

        List<Station> stations = new ArrayList<Station>(cursor.getCount());
        while(cursor.moveToNext())
        {
            stations.add(parseCursorToStation(cursor, false));
        }
        cursor.close();
        return stations;
    }

    public static boolean stationExists(long id)
    {
        Cursor cursor =
                DatabaseAccess
                        .getInstance()
                        .getReadableDatabase()
                        .rawQuery("SELECT 1 FROM " + TableStation.TABLE_NAME + " WHERE " + TableStation.ID + " = ?", new String[]{String.valueOf(id)});

        try
        {
            return cursor.getCount() > 0;
        }
        finally
        {
            cursor.close();
        }
    }

    public static Station parseCursorToStation(Cursor cursor, boolean closeCursor)
    {
        Station station = new Station();
        station.setBikeCount(cursor.getInt(cursor.getColumnIndex(TableStation.BIKE_COUNT)));
        station.setId(cursor.getLong(cursor.getColumnIndex(TableStation.ID)));
        station.setStationName(cursor.getString(cursor.getColumnIndex(TableStation.STATION_NAME)));

        LatLng location = new LatLng(cursor.getDouble(cursor.getColumnIndex(TableStation.LATITUDE)),
                                     cursor.getDouble(cursor.getColumnIndex(TableStation.LONGITUDE)));
        station.setLocation(location);

        if(closeCursor)
            cursor.close();

        return station;
    }

    public static void addStations(Station[] stations)
    {
        SQLiteDatabase db =
                DatabaseAccess
                        .getInstance()
                        .getWritableDatabase();

        StringBuilder builder = new StringBuilder();
        StringBuilder queryPlaceHolders = new StringBuilder();
        for(String column : TableStation.columns)
        {
            builder.append(column);
            builder.append(", ");
            queryPlaceHolders.append("?, ");
        }

        String sql = "INSERT INTO " + TableStation.TABLE_NAME +"(" +
                builder.toString().substring(0, builder.toString().length() - 2) + ") " +
                "VALUES (" + queryPlaceHolders.toString().substring
                (0, queryPlaceHolders.toString().length() - 2) + ");";

        db.beginTransaction();
        SQLiteStatement statement = db.compileStatement(sql);

        int maxBikes = Integer.MIN_VALUE;
        int minBikes = Integer.MAX_VALUE;

        for (Station station : stations)
        {
            if(station.getBikeCount() > maxBikes)
                maxBikes = station.getBikeCount();
            else
                minBikes = station.getBikeCount();

            if(stationExists(station.getId()))
            {
                saveStation(station);
                continue;
            }

            statement.bindLong(1, station.getId());
            statement.bindString(2, station.getStationName());
            statement.bindLong(3, station.getBikeCount());
            statement.bindDouble(4, station.getLocation().latitude);
            statement.bindDouble(5, station.getLocation().longitude);

            statement.executeInsert();
            statement.clearBindings();
        }
        db.setTransactionSuccessful();
        db.endTransaction();

        Preferences.getInstance().setValue(Preferences.KEY_MAX_BIKES, maxBikes);
        Preferences.getInstance().setValue(Preferences.KEY_MIN_BIKES, minBikes);

        EventBus.getDefault().post(new EventStationsAddedToDB());
    }

    public static final class EventStationsAddedToDB{}
}
