package com.melbikeshare;

import android.app.Application;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public class MelBikeShare extends Application
{
    private static MelBikeShare instance;

    public static MelBikeShare getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
    }
}
