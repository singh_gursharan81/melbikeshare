package com.melbikeshare;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.melbikeshare.database.helper.DBHelperStation;
import com.melbikeshare.manager.StationManager;
import com.melbikeshare.model.Station;
import com.melbikeshare.util.ImageUtils;
import com.melbikeshare.util.KeyboardUtils;
import com.melbikeshare.util.Preferences;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener
{
    private static final String TAG = "MainActivity";

    private GoogleMap mapInstance;
    private boolean mapLoaded = false;
    private boolean stationsDownloaded = false;
    private ProgressDialog pleaseWaitDialog;
    private Button getDirectionsButton;
    private Point screenSize = new Point();
    private Map<Long, Marker> stationMarkerMap;
    private Marker selectedMarker;
    private AutoCompleteTextView search;

    private final class SuggestionsAdapter extends CursorAdapter
    {
        public SuggestionsAdapter(Cursor cursor)
        {
            super(MainActivity.this, cursor, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
        {
            return LayoutInflater.from(context).inflate(R.layout.list_item_suggestions, viewGroup, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor)
        {
            Station station = DBHelperStation.parseCursorToStation(cursor, false);
            view.setTag(station);
            ((TextView)view.findViewById(R.id.listItemSuggestionsText)).setText(station.getStationName());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initialize();
    }

    private void initialize()
    {
        pleaseWaitDialog = ProgressDialog.show(this, null, "Please wait, fetching latest information...");
        StationManager.fetchAllStations();

        getWindowManager().getDefaultDisplay().getSize(screenSize);

        getDirectionsButton = (Button)findViewById(R.id.activityMainDirections);
        getDirectionsButton.setOnClickListener(this);
        findViewById(R.id.activityMainDelete).setOnClickListener(this);
        search = (AutoCompleteTextView)findViewById(R.id.activityMainSearch);

        search.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                String keyword = editable.toString();
                if (keyword.trim().length() > 0)
                    search.setAdapter(new SuggestionsAdapter(DBHelperStation.searchWithKeywordCursor(keyword)));
            }
        });
        search.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Station station = (Station) view.getTag();
                search.setText(station.getStationName());
                mapInstance.animateCamera(CameraUpdateFactory.newLatLngZoom(station.getLocation(), 16f));
                KeyboardUtils.hideKeyboard(MainActivity.this);

                (selectedMarker = stationMarkerMap.get(station.getId())).showInfoWindow();
                showDirectionsButton();
            }
        });
    }

    private void showDirectionsButton()
    {
        TranslateAnimation animation = new TranslateAnimation(screenSize.x - getDirectionsButton.getX(), 0, 0, 0);
        animation.setDuration(200);
        animation.setFillAfter(true);

        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
                getDirectionsButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                getDirectionsButton.getAnimation().reset();
                getDirectionsButton.setAnimation(null);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
        getDirectionsButton.startAnimation(animation);

    }

    private void hideDirectionsButton()
    {
        if(selectedMarker == null)
            return;

        selectedMarker.hideInfoWindow();
        selectedMarker = null;

        TranslateAnimation animation = new TranslateAnimation(0, screenSize.x - getDirectionsButton.getX(), 0, 0);
        animation.setDuration(200);
        animation.setFillAfter(true);

        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                getDirectionsButton.getAnimation().reset();
                getDirectionsButton.setAnimation(null);
                getDirectionsButton.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {

            }
        });
        getDirectionsButton.startAnimation(animation);

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(StationManager.EventStationsDownloaded downloaded)
    {
        if(downloaded.stations == null)
        {
            pleaseWaitDialog.dismiss();
            Toast.makeText(this, downloaded.errorMessage, Toast.LENGTH_LONG).show();
            plotMarkers();
        }
    }

    public void onEventMainThread(DBHelperStation.EventStationsAddedToDB addedToDB)
    {
        pleaseWaitDialog.dismiss();
        stationsDownloaded = true;
        if(mapLoaded)
            plotMarkers();

        Log.d(TAG, "Stations loaded");
    }

    private void plotMarkers()
    {
        int maxBikes = Integer.parseInt(Preferences.getInstance().getValue(Preferences.KEY_MAX_BIKES, 0).toString());
        int minBikes = Integer.parseInt(Preferences.getInstance().getValue(Preferences.KEY_MIN_BIKES, 0).toString());

        List<Station> stations = DBHelperStation.getAllStations();
        stationMarkerMap = new HashMap<Long, Marker>(stations.size());
        LatLngBounds.Builder bounds = LatLngBounds.builder();
        int outlineColour = getResources().getColor(R.color.colorPrimaryDark);
        int fillColour = getResources().getColor(R.color.colorPrimary);
        for(Station station : stations)
        {
            Bitmap iconBitmap =
                    ImageUtils.getCircularBitmap(maxBikes, minBikes,
                                                 station.getBikeCount(), outlineColour, fillColour);

            Marker marker = mapInstance.addMarker(new MarkerOptions()
                                                   .position(station.getLocation())
                                                   .icon(BitmapDescriptorFactory.fromBitmap(iconBitmap))
                                                   .title(station.getStationName()));
            stationMarkerMap.put(station.getId(), marker);
            bounds.include(station.getLocation());
        }
        mapInstance.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 110));
        mapInstance.setOnCameraChangeListener(null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mapInstance = googleMap;
        mapInstance.setMyLocationEnabled(true);
        mapInstance.getUiSettings().setMapToolbarEnabled(false);
        mapInstance.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener()
        {
            @Override
            public void onCameraChange(CameraPosition cameraPosition)
            {
                mapLoaded = true;
                if (stationsDownloaded)
                    plotMarkers();
            }
        });
        mapInstance.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker)
            {
                showDirectionsButton();
                marker.showInfoWindow();
                mapInstance.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 16f));
                selectedMarker = marker;
                return true;
            }
        });
        mapInstance.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng latLng)
            {
                hideDirectionsButton();
            }
        });
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.activityMainDelete:
                search.setText(null);
                hideDirectionsButton();
                return;

            case R.id.activityMainDirections:
                Location myLocation = mapInstance.getMyLocation();
                if(myLocation == null)
                {
                    Toast.makeText(this, "Could not fetch you current location, make sure you have an internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                String directionsUrl =
                        "http://maps.google.com/maps?daddr=" +
                                myLocation.getLatitude() + "," + myLocation.getLongitude() +
                                "&saddr=" + selectedMarker.getPosition().latitude + "," +
                                selectedMarker.getPosition().longitude;

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(directionsUrl));
                startActivity(intent);
        }
    }
}
