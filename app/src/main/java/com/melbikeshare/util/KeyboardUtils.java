package com.melbikeshare.util;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils
{
    public static void showKeyboard(Activity activity)
    {
        InputMethodManager inputManager = (InputMethodManager)activity.
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(Activity activity)
    {
        if(activity == null || activity.getCurrentFocus() == null)
            return;

        InputMethodManager inputManager = (InputMethodManager) activity.
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                                             InputMethodManager.HIDE_NOT_ALWAYS);
    }
}