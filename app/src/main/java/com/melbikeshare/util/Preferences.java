package com.melbikeshare.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.melbikeshare.MelBikeShare;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public final class Preferences
{
    public static final String KEY_MAX_BIKES = "maxBikes";
    public static final String KEY_MIN_BIKES = "minBikes";

    private static Preferences instance;
    private static SharedPreferences sharedPreferences;

    private Preferences()
    {
        sharedPreferences =
                MelBikeShare
                        .getInstance()
                        .getSharedPreferences("MelBikeSharePreferences", Context.MODE_PRIVATE);
    }

    public static Preferences getInstance()
    {
        if(instance == null || sharedPreferences == null)
        {
            instance = new Preferences();
        }
        return instance;
    }

    public void setValue(String key, Object value)
    {
        if(value instanceof String)
        {
            sharedPreferences.edit().putString(key, value.toString()).apply();
        }
        else if(value instanceof Integer)
        {
            sharedPreferences.edit().putInt(key, Integer.valueOf(value.toString())).apply();;
        }
        else if(value instanceof Float)
        {
            sharedPreferences.edit().putFloat(key, Float.valueOf(value.toString())).apply();;
        }
        else if(value instanceof Boolean)
        {
            sharedPreferences.edit().putBoolean(key, Boolean.valueOf(value.toString())).apply();;
        }
        else if(value instanceof Long)
        {
            sharedPreferences.edit().putLong(key, Long.valueOf(value.toString())).apply();;
        }
    }

    public Object getValue(String key, Object defaultValue)
    {
        Object value = sharedPreferences.getAll().get(key);
        return value == null ? defaultValue : value;
    }
}
