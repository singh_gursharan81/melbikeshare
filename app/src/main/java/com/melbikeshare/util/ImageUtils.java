package com.melbikeshare.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public final class ImageUtils
{
    private ImageUtils(){}

    /**
     * @param maxBikes the highest number of bikes available in any station
     * @param minBikes the lowest number of bikes available at any station
     * @param givenBikes the given number of bikes for which the marker is being drawn
     * @param strokeColour the colour of the outline of the circle
     * @param fillColour the fill colour of the circle
     * @return the generated bitmap
     */
    public static Bitmap getCircularBitmap(int maxBikes, int minBikes, int givenBikes, int strokeColour, int fillColour)
    {
        int optimumSize = (int)(givenBikes/(float)(maxBikes + minBikes) * 50) + 50;
        Bitmap output = Bitmap.createBitmap(optimumSize,
                                            optimumSize, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();

        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(strokeColour);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        canvas.drawCircle(optimumSize / 2, optimumSize / 2,
                          optimumSize / 2 - 5, paint);

        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(fillColour);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(optimumSize / 2, optimumSize / 2, optimumSize / 2 - 7.5f, paint);

        paint.setColor(Color.WHITE);
        paint.setTextSize(20);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(String.valueOf(givenBikes), optimumSize/2, optimumSize/2 + paint.descent(), paint);
        return output;
    }
}
