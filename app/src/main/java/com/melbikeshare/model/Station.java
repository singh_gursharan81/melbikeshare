package com.melbikeshare.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by gursharan_singh on 9/11/2015.
 */
public class Station
{
    private static final String SERIALIZED_NAME_ID = "id";
    private static final String SERIALIZED_NAME_COORDINATES = "coordinates";
    private static final String SERIALIZED_NAME_FEATURE_NAME = "featurename";
    private static final String SERIALIZED_NAME_NB_EMPTY_DOC = "nbemptydoc";
    private static final String JSON_COORDINATES = "coordinates";

    @SerializedName(SERIALIZED_NAME_ID)
    private long id;

    @SerializedName(SERIALIZED_NAME_COORDINATES)
    private Object coordinates;

    @SerializedName(SERIALIZED_NAME_FEATURE_NAME)
    private String stationName;

    @SerializedName(SERIALIZED_NAME_NB_EMPTY_DOC)
    private int bikeCount;

    private LatLng location;

    public long getId()
    {
        return id;
    }

    public LatLng getLocation()
    {
        if(location == null)
        {
            try
            {
                JSONObject object = new JSONObject(coordinates.toString());
                JSONArray coordinates = object.getJSONArray(JSON_COORDINATES);
                location = new LatLng(coordinates.getDouble(1), coordinates.getDouble(0));
            }
            catch(JSONException e)
            {
                e.printStackTrace();
            }
        }

        return location;
    }

    public String getStationName()
    {
        return stationName;
    }

    public int getBikeCount()
    {
        return bikeCount;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public void setStationName(String stationName)
    {
        this.stationName = stationName;
    }

    public void setBikeCount(int bikeCount)
    {
        this.bikeCount = bikeCount;
    }

    public void setLocation(LatLng location)
    {
        this.location = location;
        try
        {
            JSONArray coordinatesArray =
                    new JSONArray()
                            .put(location.longitude)
                            .put(location.latitude);

            coordinates =
                    new JSONObject()
                            .put("type", "Point")
                            .put(SERIALIZED_NAME_COORDINATES, coordinatesArray);;
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public String toString()
    {
        return stationName;
    }
}
