package com.melbikeshare.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by gursharan_singh on 8/11/2015.
 */
public final class NetworkCalls
{
    public static final String SODA_API_ALL_STATIONS = "https://data.melbourne.vic.gov.au/resource/qnjw-wgaj.json?$select=coordinates,featurename,nbemptydoc,id";
    public static final String EXCEPTION_IO = "Could not access network, please check your connection";
    public static final String EXCEPTION_PROTOCOL = "A protocol error occurred";
    public static final String EXCEPTION_MALFORMED_URL = "The data source does not seem to exist anymore";

    private NetworkCalls(){}

    private static String convertStreamToString(InputStream inputStream) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = reader.readLine()) != null)
        {
            sb.append(line);
            sb.append("\n");
        }
        inputStream.close();
        return sb.toString();
    }

    public static String getRequest(String url)
    {
        try
        {
            HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
            connection.setRequestMethod("GET");
            if(connection.getResponseCode() != 200)
                throw new RuntimeException("Error: check to make sure URL is valid");
            InputStream inputStream = connection.getInputStream();
            return convertStreamToString(inputStream);
        }
        catch(MalformedURLException malformed)
        {
            malformed.printStackTrace();
            return EXCEPTION_MALFORMED_URL;
        }
        catch(ProtocolException protocol)
        {
            protocol.printStackTrace();
            return EXCEPTION_PROTOCOL;
        }
        catch(IOException exception)
        {
            exception.printStackTrace();
            return EXCEPTION_IO;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
