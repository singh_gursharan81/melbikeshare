package com.melbikeshare.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.melbikeshare.MelBikeShare;
import com.melbikeshare.manager.StationManager;

public class NetworkMonitor extends BroadcastReceiver
{
    private static final String TAG = "NetworkMonitor";

    @Override
    public void onReceive(Context context, Intent intent )
    {
        Log.d(TAG, "Connected to internet");
        if(isConnected(context))
        {
            Toast.makeText(MelBikeShare.getInstance(), "Connected to the internet, fetching latest information...", Toast.LENGTH_SHORT).show();
            StationManager.fetchAllStations();
        }
    }

    public static boolean isConnected(Context context)
    {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
